package pl.serioteam.hackyeah.goster.utils

import lombok.`val`

//
//import android.annotation.SuppressLint
//import android.app.Activity
//import android.content.IntentSender
//import android.location.Location
//import android.os.Looper
//import android.util.Log
//import com.google.android.gms.location.*
//import com.google.android.gms.tasks.OnFailureListener
//import com.google.android.gms.tasks.OnSuccessListener
//
//import lombok.val;
//import java.text.DateFormat
//import java.util.*
//
//
class LocationService {

    fun getArrayOfGps(): FloatArray {
        return floatArrayOf(0f, 0.1f, 0.3f, 0f, 0.5f, 0f, 0.1f, 0.2f, 0f, 0.1f, 0.3f, 0f, 0.5f,
                0f, 0.1f, 0.2f, 0.3f, 5f, 8f, 14f, 21f, 25f, 25.6f, 29f,
                31f, 32.6f,  33f, 33f, 33f, 31f, 28f, 14f, 0.5f, 0f, 0.1f, 0.2f, 0f, 0.1f, 0.3f, 0f, 0f, 0f, 0f, 0.5f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f)
    }
}
//    private val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 1000
//    private val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2
//
//    private var currentLongitiude : Double = 0.0
//    private var currentLatitude : Double = 0.0
//
//    private lateinit var mFusedLocationClient: FusedLocationProviderClient
//    private lateinit var mSettingsClient: SettingsClient
//    private lateinit var mLocationRequest: LocationRequest
//    private var mLocationSettingsRequest: LocationSettingsRequest? = null
//    private var mLocationCallback: LocationCallback? = null
//    private var mCurrentLocation: Location? = null
//
//    private var mLastUpdateTime: String? = null
//
//    private lateinit var activity : Activity
//
//    fun start(context: Activity) {
//        this.activity = context
//        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
//        mSettingsClient = LocationServices.getSettingsClient(context)
//
//        createLocationCallback()
//        createLocationRequest()
//        buildLocationSettingsRequest()
//        startLocationUpdates()
//    }
//
//    private fun createLocationRequest() {
//        mLocationRequest = LocationRequest()
//
//        // Sets the desired interval for active location updates. This interval is
//        // inexact. You may not receive updates at all if no location sources are available, or
//        // you may receive them slower than requested. You may also receive updates faster than
//        // requested if other applications are requesting location at a faster interval.
//        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)
//
//        // Sets the fastest rate for active location updates. This interval is exact, and your
//        // application will never receive updates faster than this value.
//        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS)
//
//        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
//    }
//
//    /**
//     * Creates a callback for receiving location events.
//     */
//    private fun createLocationCallback() {
//        mLocationCallback = object : LocationCallback() {
//            override fun onLocationResult(locationResult: LocationResult) {
//                super.onLocationResult(locationResult)
//
//                mCurrentLocation = locationResult.lastLocation
//                mLastUpdateTime = DateFormat.getTimeInstance().format(Date())
//
//                Log.d("TEST", "${mCurrentLocation.toString()} ${DateFormat.getTimeInstance().calendar.timeInMillis}")
//            }
//        }
//    }
//
//
//
//    @SuppressLint("MissingPermission")
//    private fun startLocationUpdates() {
//        // Begin by checking if the device has the necessary location settings.
//        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
//                .addOnSuccessListener(activity, OnSuccessListener<LocationSettingsResponse> {
//                    Log.i("test", "All location settings are satisfied.")
//
//                    mFusedLocationClient.requestLocationUpdates(mLocationRequest,
//                            mLocationCallback, Looper.myLooper())
//
//                })
//                .addOnFailureListener(activity, OnFailureListener { e ->
//                    val statusCode = (e as ApiException).statusCode
//                    when (statusCode) {
//                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
//                            Log.i("test", "Location settings are not satisfied. Attempting to upgrade " + "location settings ")
//                            try {
//                                // Show the dialog by calling startResolutionForResult(), and check the
//                                // result in onActivityResult().
//                            } catch (sie: IntentSender.SendIntentException) {
//                                Log.i("test", "PendingIntent unable to execute request.")
//                            }
//
//                        }
//                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
//                            val errorMessage = "Location settings are inadequate, and cannot be " + "fixed here. Fix in Settings."
//                            Log.e("test", errorMessage)
//                        }
//                    }
//
//                })
//    }
//
//    /**
//     * Uses a [com.google.android.gms.location.LocationSettingsRequest.Builder] to build
//     * a [com.google.android.gms.location.LocationSettingsRequest] that is used for checking
//     * if a device has the needed location settings.
//     */
//    private fun buildLocationSettingsRequest() {
//        val builder = LocationSettingsRequest.Builder()
//        builder.addLocationRequest(mLocationRequest)
//        mLocationSettingsRequest = builder.build()
//    }
//
//    fun calculateDistance(location1: LocationData, location2: LocationData): Double {
//        return meterDistanceBetweenPoints(
//                location1.latitude,
//                location1.longitude,
//                location2.latitude,
//                location2.longitude)
//    }
//
//    private fun meterDistanceBetweenPoints(lat_a: Float, lng_a: Float, lat_b: Float, lng_b: Float): Double {
//        val pk = (180f / Math.PI).toFloat()
//
//        val a1 = lat_a / pk
//        val a2 = lng_a / pk
//        val b1 = lat_b / pk
//        val b2 = lng_b / pk
//
//        val t1 = Math.cos(a1.toDouble()) * Math.cos(a2.toDouble()) * Math.cos(b1.toDouble()) * Math.cos(b2.toDouble())
//        val t2 = Math.cos(a1.toDouble()) * Math.sin(a2.toDouble()) * Math.cos(b1.toDouble()) * Math.sin(b2.toDouble())
//        val t3 = Math.sin(a1.toDouble()) * Math.sin(b1.toDouble())
//        val tt = Math.acos(t1 + t2 + t3)
//
//        return 6366000 * tt
//    }
