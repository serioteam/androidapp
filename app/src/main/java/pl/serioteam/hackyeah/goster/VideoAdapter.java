package pl.serioteam.hackyeah.goster;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.Setter;


public class VideoAdapter extends RecyclerView.Adapter<VideoHolder> {
    private final LayoutInflater inflater;
    private List<Video> videos;

    @Setter
    private OnVideoItemClickedListener onClickListener;

    public VideoAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        this.videos = new ArrayList<>();
    }

    @Override
    public VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.goster_item, parent, false);
        VideoHolder viewHolder = new VideoHolder(view);
        viewHolder.setOnClickListener(onClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(VideoHolder holder, int position) {
        Video video = videos.get(position);
        holder.setItem(video);
    }

    @Override
    public int getItemCount() {
        return videos.size();
    }

    public void updateVideo(List<Video> list) {
        videos = list;
        notifyDataSetChanged();
    }

    public interface OnVideoItemClickedListener {
        void onVideoItemClicked(Video video);
    }
}