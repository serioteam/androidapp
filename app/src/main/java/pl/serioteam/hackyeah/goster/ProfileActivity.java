package pl.serioteam.hackyeah.goster;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.navigationProfile)
    ImageView navigateIcon;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.details_speed)
    ImageView speadDetail;

    @BindView(R.id.topProfile)
    ImageView topProfile;

    VideoAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        ButterKnife.bind(this);

        Glide.with(this).load(R.drawable.top_profil).into(topProfile);
        Glide.with(this).load(R.drawable.profil_dane).into(speadDetail);

        navigateIcon.setImageDrawable(getDrawable(R.drawable.ic_profile_visible));
    }

    @Override
    protected void onResume() {
        super.onResume();
        initRecycler();
    }

    private void initRecycler() {
        adapter = new VideoAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        List<Video> list = PreferencesHelper.getVideoList();

        adapter.updateVideo(list);
    }


    @OnClick(R.id.navigationAdd)
    void navigationAdd() {
        navigateToActivity(AddActivity.class);
    }
    @OnClick(R.id.navigationMaps)
    void navigationMaps() {
        navigateToActivity(MapActivity.class);
    }
    @OnClick(R.id.navigationTimeLine)
    void navigationTimeLine() {
        navigateToActivity(ForumActivity.class);
    }
    @OnClick(R.id.navigationFavorite)
    void navigationFavorite() {
        navigateToActivity(FavoriteActivity.class);
    }
    @OnClick(R.id.navigationProfile)
    void navigationProfile() {
        navigateToActivity(ProfileActivity.class);
    }

    private void navigateToActivity(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(intent);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }
}
