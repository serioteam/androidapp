package pl.serioteam.hackyeah.goster;


import java.util.List;

public class FavoriteMovieHelper {

    public static boolean changeFavorite(Video video) {

        boolean isFavorite = isFavorite(video);

        if(isFavorite) {
            PreferencesHelper.deleteFromFavorites(video);
        } else {
            PreferencesHelper.addToFavorites(video);
        }
        return !isFavorite;
    }

    public static boolean isFavorite(Video video) {

        boolean isFavorite = false;
        List<Video> videoList = PreferencesHelper.getFavoriteList();
        for(Video v: videoList) {
            if(v.getId().equals(video.getId())) {
                isFavorite = true;
            }
        }
        return isFavorite;
    }

}
