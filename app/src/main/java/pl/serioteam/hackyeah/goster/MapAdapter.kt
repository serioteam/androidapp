package pl.serioteam.hackyeah.goster

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.google.android.gms.maps.model.LatLng
import java.util.*

class MapAdapter : RecyclerView.Adapter<MapViewHolder>() {

    companion object {
        val values = mapOf<Int, String>(
                R.drawable.kafelek_video_det to "Jazda z górki",
                R.drawable.kafelek_profil_1 to "Super przejazd",
                R.drawable.kafelek_profil_2 to "Jazda z górki",
                R.drawable.video05 to "Super przejazd",
                R.drawable.video06 to "Jazda z górki",
                R.drawable.video01 to "Super przejazd",
                R.drawable.video02 to "Jazda z górki",
                R.drawable.video03 to "Super przejazd",
                R.drawable.video04 to "Jazda z górki"
        )
    }

    var data: List<LatLng>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = data?.size ?: 0

    override fun onBindViewHolder(holder: MapViewHolder?, position: Int) {
        holder?.image = values.keys.elementAt(position)
        holder?.title = values.get(holder?.image ?: 0)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MapViewHolder {
        val view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.map_element, parent, false)

        val viewHolder = MapViewHolder(view)
        view.setOnClickListener { viewHolder.onClick() }
        return viewHolder
    }

}

class MapViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    @BindView(R.id.item_distance)
    lateinit var distance: TextView

    @BindView(R.id.item_name)
    lateinit var name: TextView

    @BindView(R.id.item_image)
    lateinit var imageView: ImageView

    @BindView(R.id.item_tries)
    lateinit var tries: TextView

    var image: Int? = null
        set(value) {
            field = value
            imageView.setImageResource(image ?: 0)
        }
    var title: String? = null
        set(value) {
            field = value
            name.text = title
        }

    init {
        ButterKnife.bind(this, itemView)
        distance.text = "2,5km"
        tries.text = "${Random().nextInt(40)} tries"
    }

    fun onClick() {
        val intent = Intent(itemView.context, DetailsActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        intent.putExtra("image", image)
        intent.putExtra("title", title)
        itemView.context.startActivity(intent)
    }
}
