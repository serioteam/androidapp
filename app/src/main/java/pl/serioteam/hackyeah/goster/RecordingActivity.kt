package pl.serioteam.hackyeah.goster

import android.content.Intent
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import java.util.*
import java.util.concurrent.TimeUnit

class RecordingActivity : AppCompatActivity(), TextToSpeech.OnInitListener {

    @BindView(R.id.recording_counter)
    lateinit var counter: TextView

    val compositeDisposable: CompositeDisposable = CompositeDisposable()
    val timeout = 1000L
    var counterInit = 0

    lateinit var tts: TextToSpeech

    val list = arrayOf("Your biggest opponent isn’t the other guy",
            "Do not let what you can not do interfere with what you can do",
            "Never give up! Failure and rejection are only the first step to succeeding",
            "ust keep going. Everybody gets better if they keep at it",
            "It ain’t over till it’s over",
            "It’s hard to beat a person who never gives up",
            "Without self-discipline, success is impossible, period",
            "The will to win is important, but the will to prepare is vital",
            "It is more difficult to stay on top than to get there")

//    val timeList = arrayOf()

    override fun onInit(status: Int) {
        if(status == TextToSpeech.SUCCESS){
            val result = tts.setLanguage(Locale.US);
            if(result== TextToSpeech.LANG_MISSING_DATA ||
                    result== TextToSpeech.LANG_NOT_SUPPORTED){
                Log.e("error", "This Language is not supported");
            }
            else {
                var counter = 0
                Log.d("test", "TESTESTEST")
                compositeDisposable.add(Observable
                        .interval(0L, 5L, TimeUnit.SECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            counter += 1
                            if(counter % 5 != 0) {
                                val text = if (Random().nextBoolean()) {
                                    "behind"
                                } else {
                                    "ahead"
                                }
                                tts.speak("You are ${Random().nextInt(8) + 1} meters $text", TextToSpeech.QUEUE_ADD, null, null)
                            } else {
                                tts.speak(list[Random(System.currentTimeMillis()).nextInt(list.size)], TextToSpeech.QUEUE_ADD, null, null)
                            }
                        })
            }
        }
        else
            Log.e("error", "Initilization Failed!")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recording)
        ButterKnife.bind(this)

        tts = TextToSpeech(this, this)

        compositeDisposable.add(
                Observable.interval(timeout, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread()).subscribe({
                    ++counterInit
                    val seconds = counterInit % 60
                    val minutes = counterInit / 60 % 3600
                    val hours = counterInit / 3600 % 6000
                    val str = "${String.format("%02d", hours)}:${String.format("%02d", minutes)}:${String.format("%02d", seconds)}"
                    counter.text = str
                }, {})
        )
    }

    @OnClick(R.id.recording_stop)
    fun recStop() {
        compositeDisposable.clear()
        startActivity(Intent(this, UploadActivity::class.java))
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}