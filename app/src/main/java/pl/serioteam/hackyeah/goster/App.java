package pl.serioteam.hackyeah.goster;

import android.app.Application;

import com.orhanobut.hawk.Hawk;

import java.util.List;

import lombok.Getter;

public class App extends Application {

    @Getter
    private static App instance = new App();

    @Override
    public void onCreate() {
        super.onCreate();
        initHawk();
        instance = this;
    }

    private void initHawk() {
        Hawk.init(this).build();

        List<Video> list = PreferencesHelper.getVideoList();
        if(list.isEmpty()) {
            list.add(new Video(R.drawable.video04, "Piotr Liszka"));
            list.add(new Video(R.drawable.video01, "John Done"));
            list.add(new Video(R.drawable.video02, "Dominik Jakistam"));
            list.add(new Video(R.drawable.video03, "Dominik Jakistam"));
            PreferencesHelper.saveVideosList(list);
        }
    }
}