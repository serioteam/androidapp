package pl.serioteam.hackyeah.goster

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Observable.just(true)
                .delay(4000L, TimeUnit.MILLISECONDS)
                .doOnError {  }
                .subscribe {
                    navigateToActivity(LoginActivity::class.java)
                }
    }

    private fun navigateToActivity(cls: Class<*>) {
        val intent = Intent(this, cls)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK

        startActivity(intent)
        overridePendingTransition(0, 0)
    }
}