package pl.serioteam.hackyeah.goster

import android.Manifest
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.w3c.dom.Document
import pl.serioteam.hackyeah.goster.utils.GMapV2Direction


class MapActivity : AppCompatActivity() {

    @BindView(R.id.map_carousel)
    lateinit var carousel: RecyclerView

    var adapter: MapAdapter = MapAdapter()

    @BindView(R.id.navigationMaps)
    lateinit var navigateIcon: ImageView

    companion object {
        val cracowLatLang = LatLng(50.067676, 19.990742)
        val routeMap = mapOf<LatLng, LatLng>(
                LatLng(50.068482, 19.988425) to LatLng(50.070268, 19.983260),
                LatLng(50.064412, 19.989353) to LatLng(50.070844, 20.004671),
                LatLng(50.079376, 19.999661) to LatLng(50.073317, 19.986752),
                LatLng(50.068707, 19.979776) to LatLng(50.066295, 19.961375),
                LatLng(50.058688, 19.962049) to LatLng(50.063883, 19.987965),
                LatLng(50.052441, 19.989121) to LatLng(50.050523, 20.012628),
                LatLng(50.058750, 20.023900) to LatLng(50.064193, 20.015711),
                LatLng(50.072974, 20.040375) to LatLng(50.078909, 20.065231),
                LatLng(50.092509, 20.040664) to LatLng(50.101532, 20.069759)
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        ButterKnife.bind(this)

        adapter.data = ArrayList()
        carousel.adapter = adapter
        carousel.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        navigateIcon.setImageDrawable(getDrawable(R.drawable.ic_map_visible))

        RxPermissions(this)
                .request(Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe({ granted ->
                    if (granted) {
                        (supportFragmentManager?.findFragmentById(R.id.map_fragment) as SupportMapFragment)
                                .getMapAsync {
                                    setupMap(it)
                                }
                    } else {
                        finish()
                    }
                })
    }

    private fun setupMap(map: GoogleMap?) {
        getRoutes(map)
                .subscribe(
                        {
                            map?.apply {
                                isMyLocationEnabled = true
                                mapType = GoogleMap.MAP_TYPE_NORMAL
                                setMapStyle(MapStyleOptions.loadRawResourceStyle(this@MapActivity, R.raw.map_style))
                                moveCamera(CameraUpdateFactory.newLatLngZoom(cracowLatLang, 25f))
                                animateCamera(CameraUpdateFactory.zoomTo(14f), 2000, null)
                                setOnCameraChangeListener({ currentPosition ->
                                    adapter.data = routeMap.filter {
                                        map.projection.visibleRegion.latLngBounds.contains(it.key)
                                                && map.projection.visibleRegion.latLngBounds.contains(it.value)
                                    }.keys.toList()
                                })

                                it.forEach { addPolyline(it) }
                            }
                        }, {}
                )
    }

    private fun getRoutes(map: GoogleMap?): Observable<List<PolylineOptions>> {
        val gmap = GMapV2Direction()
        var result = ArrayList<PolylineOptions>()
        var polyline: PolylineOptions
        var observables = ArrayList<Observable<Document>>()

        routeMap.forEach { from, to ->
            map?.addMarker(
                    MarkerOptions()
                            .position(from)
                            .anchor(.5f, .5f)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.dot)))
            map?.addMarker(
                    MarkerOptions()
                            .position(to)
                            .anchor(.5f, .5f)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.dot)))
            observables.add(
                    Flowable
                            .fromCallable {
                                gmap
                                        .getDocument(from, to, GMapV2Direction.MODE_WALKING)
                            }
                            .subscribeOn(Schedulers.computation())
                            .observeOn(AndroidSchedulers.mainThread())
                            .toObservable()
            )
        }

        return Observable
                .merge(observables)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .map({
                    polyline = PolylineOptions().width(8f).color(Color.RED)
                    polyline.addAll(gmap.getDirection(it))
                    result.add(polyline)
                    return@map result
                }
                )

    }

    @OnClick(R.id.navigationAdd)
    internal fun navigationAdd() {
        navigateToActivity(AddActivity::class.java)
    }

    @OnClick(R.id.navigationMaps)
    internal fun navigationMaps() {
        navigateToActivity(MapActivity::class.java);
    }

    @OnClick(R.id.navigationTimeLine)
    internal fun navigationTimeLine() {
        navigateToActivity(ForumActivity::class.java)
    }

    @OnClick(R.id.navigationFavorite)
    internal fun navigationFavorite() {
        navigateToActivity(FavoriteActivity::class.java)
    }

    @OnClick(R.id.navigationProfile)
    internal fun navigationProfile() {
        navigateToActivity(GhosterActivity::class.java)
    }

    private fun navigateToActivity(cls: Class<*>) {
        val intent = Intent(this, cls)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK

        startActivity(intent)
        overridePendingTransition(R.anim.fadein, R.anim.fadeout)
    }
}