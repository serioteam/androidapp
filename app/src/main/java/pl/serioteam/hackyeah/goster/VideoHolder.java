package pl.serioteam.hackyeah.goster;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.Setter;

class VideoHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.videoImage)
    ImageView image;

    @BindView(R.id.playVideo)
    View playVideo;

    @BindView(R.id.favorite)
    ImageView favorite;

    private final Context context;
    private Video video;


    @Override
    public String toString() {
        return super.toString();
    }

    @Setter
    private VideoAdapter.OnVideoItemClickedListener onClickListener;

    VideoHolder(View itemView) {
        super(itemView);
        this.context = itemView.getContext();
        ButterKnife.bind(this, itemView);
    }

    void setItem(Video video) {
        this.video = video;
        setImage(video.getId());
        isFavorite();
    }

    private void isFavorite() {
        if(FavoriteMovieHelper.isFavorite(video)) {
            favorite.setImageDrawable(context.getDrawable(R.drawable.ic_like_red));
        } else {
            favorite.setImageDrawable(context.getDrawable(R.drawable.ic_like));
        }
    }

    private void setImage(int imageUrl) {
        if(imageUrl==0) {
            imageUrl = R.drawable.video02;
        }
        Glide.clear(image);
        Glide.with(context).load(imageUrl).into(image);
    }

    @OnClick(R.id.videoImage)
    void onImageClicked() {

        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra("title", video.getUser());
        context.startActivity(intent);
    }

    @OnClick(R.id.favorite)
    void onClick() {
        FavoriteMovieHelper.changeFavorite(video);
        isFavorite();
    }
}