package pl.serioteam.hackyeah.goster

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit

class RecordingPreparationActivity : AppCompatActivity() {

    @BindView(R.id.recording_preparation_counter)
    lateinit var counter: TextView

    val compositeDisposable: CompositeDisposable = CompositeDisposable()
    val timeout = 1000L
    var counterInit = 5

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recording_preparation)
        ButterKnife.bind(this)
        counter.text = counterInit.toString()
        compositeDisposable.add(
            Observable.interval(timeout, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread()).subscribe({
                if (--counterInit < 0) {
                    startActivity(Intent(this, RecordingActivity::class.java))
                    compositeDisposable.clear()
                } else {
                    counter.text = counterInit.toString()
                }
            }, {})
        )
    }


}