package pl.serioteam.hackyeah.goster;

import android.support.annotation.NonNull;

import java.util.Comparator;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Video {

    public Video(int id, String user) {
        this.id = id;
        this.user = user;
    }

    Integer id;
    String user;
    String image;

    @Override
    public String toString() {
        return id.toString();
    }
}
