package pl.serioteam.hackyeah.goster

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick

class UploadActivity : AppCompatActivity() {

    @BindView(R.id.upload_progress_bar)
    lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload)
        ButterKnife.bind(this)
    }

    override fun onResume() {
        super.onResume()
        progressBar.visibility = View.GONE
    }

    @OnClick(R.id.upload_confirm)
    fun confirm() {
        progressBar.visibility = View.VISIBLE
        Handler().postDelayed({
            Toast.makeText(this, "Upload completed", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, ForumActivity::class.java))
        }, 3500)
    }

    @OnClick(R.id.upload_cancel)
    fun cancel() {
        startActivity(Intent(this, ForumActivity::class.java))
    }
}