package pl.serioteam.hackyeah.goster

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide

class GhosterActivity : AppCompatActivity() {

    @BindView(R.id.navigationProfile)
    lateinit var navigateIcon: ImageView

    @BindView(R.id.recyclerView)
    lateinit var recyclerView : RecyclerView

    @BindView(R.id.details_speed)
    lateinit var speadDetail : ImageView

    @BindView(R.id.topProfile)
    lateinit var topProfile : ImageView

    lateinit var adapter: VideoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ghoster)
        ButterKnife.bind(this)

        Glide.with(this).load(R.drawable.top_profil).into(topProfile)
        Glide.with(this).load(R.drawable.profil_dane).into(speadDetail)

        navigateIcon.setImageDrawable(getDrawable(R.drawable.ic_profile_visible))
    }



    override fun onResume() {
        super.onResume()
        initRecycler()
    }

    private fun initRecycler() {
        adapter = VideoAdapter(this)
        recyclerView.setLayoutManager(LinearLayoutManager(this))
        recyclerView.setAdapter(adapter)
        val list = PreferencesHelper.getFavoriteList()
        adapter.updateVideo(list)
    }

    @OnClick(R.id.navigationAdd)
    internal fun navigationAdd() {
        navigateToActivity(AddActivity::class.java)
    }

    @OnClick(R.id.navigationMaps)
    internal fun navigationMaps() {
        navigateToActivity(MapActivity::class.java);
    }

    @OnClick(R.id.navigationTimeLine)
    internal fun navigationTimeLine() {
        navigateToActivity(ForumActivity::class.java)
    }

    @OnClick(R.id.navigationFavorite)
    internal fun navigationFavorite() {
        navigateToActivity(FavoriteActivity::class.java)
    }

    @OnClick(R.id.navigationProfile)
    internal fun navigationProfile() {
        navigateToActivity(ProfileActivity::class.java)
    }

    private fun navigateToActivity(cls: Class<*>) {
        val intent = Intent(this, cls)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK

        startActivity(intent)
        overridePendingTransition(0, 0)
    }
}