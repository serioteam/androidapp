package pl.serioteam.hackyeah.goster;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddActivity extends AppCompatActivity {

    @BindView(R.id.navigationAdd)
    ImageView navigateIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        ButterKnife.bind(this);

        navigateIcon.setImageDrawable(getDrawable(R.drawable.ic_search_visible));

    }

    @OnClick(R.id.navigationAdd)
    void navigationAdd() {
        navigateToActivity(AddActivity.class);
    }
    @OnClick(R.id.navigationMaps)
    void navigationMaps() {
        navigateToActivity(MapActivity.class);
    }
    @OnClick(R.id.navigationTimeLine)
    void navigationTimeLine() {
        navigateToActivity(ForumActivity.class);
    }
    @OnClick(R.id.navigationFavorite)
    void navigationFavorite() {
        navigateToActivity(FavoriteActivity.class);
    }
    @OnClick(R.id.navigationProfile)
    void navigationProfile() {
        navigateToActivity(ProfileActivity.class);
    }

    private void navigateToActivity(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }
}
