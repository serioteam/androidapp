package pl.serioteam.hackyeah.goster

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.TextView
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide

class DetailsActivity : AppCompatActivity() {

    @BindView(R.id.details_title)
    lateinit var title: TextView

    @BindView(R.id.detailsImage)
    lateinit var detailsImage : ImageView

    @BindView(R.id.details_goster)
    lateinit var detailsGoster : ImageView

    @BindView(R.id.details_speed)
    lateinit var detailsSpeed : ImageView

    @BindView(R.id.details_map)
    lateinit var detailsMap : ImageView

    @BindView(R.id.playVideo)
    lateinit var playVideo: View


    @BindView(R.id.details_challenge_accepted)
    lateinit var challangeAccepted: RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        ButterKnife.bind(this)

        val titleText = intent.getStringExtra("title")

        if(titleText.equals("Piotr Liszka")) {

            Glide.with(this).load(R.drawable.kafelek_video_piotr).into(detailsImage)
            Glide.with(this).load(R.drawable.goster_piotr).into(detailsGoster)
            Glide.with(this).load(R.drawable.mapka_mini).into(detailsMap)
            Glide.with(this).load(R.drawable.speed_piotr).into(detailsSpeed)
            playVideo.setOnClickListener { startActivity(Intent(this, VideoActivity::class.java)) }
            title.text = "Trasa trudna"
        } else {
            Glide.with(this).load(R.drawable.kafelek_video_det).into(detailsImage)
            Glide.with(this).load(R.drawable.goster).into(detailsGoster)
            Glide.with(this).load(R.drawable.mapka_mini).into(detailsMap)
            Glide.with(this).load(R.drawable.speed).into(detailsSpeed)
            title.text = titleText;
        }

    }



    @OnClick(R.id.details_map)
    fun navigateToMap() {
        startActivity(Intent(this, MapActivity::class.java))
    }

    @OnClick(R.id.details_goster)
    fun ghosterClicked() {
        Log.e("", "dupa wołowa")
        startActivity(Intent(this, GhosterActivity::class.java))
    }

    @OnClick(R.id.details_go_button)
    fun goButtonClicked() {
        challangeAccepted.visibility = View.VISIBLE
    }

    @OnClick(R.id.details_back)
    fun back() {
        super.onBackPressed()
    }

    @OnClick(R.id.details_ok)
    fun detailsOk() {
        Toast.makeText(this, "Thanks!", Toast.LENGTH_SHORT).show()
    }

    @OnClick(R.id.challenge_accepted_go)
    fun challange_go() {
        startActivity(Intent(this, RecordingPreparationActivity::class.java))
    }
}