package pl.serioteam.hackyeah.goster;

import android.util.Log;

import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PreferencesHelper {

    public static final String VIDEOS = "videos";
    public static final String FAVORITES = "favorites";

    public static void saveVideosList(List<Video> videosList) {
        Hawk.put(VIDEOS, videosList);
    }

    public static List<Video> getVideoList() {
        return Hawk.get(VIDEOS, new ArrayList<Video>());

    }

    public static void deleteFromFavorites(Video video) {
        List<Video> list = getFavoriteList();
        Iterator<Video> i = list.iterator();
        while (i.hasNext()) {
            Video v = i.next();
            if(v.getId().equals(video.getId())) {
                i.remove();
            }
        }
        saveFavoriteList(list);
    }

    public static void addToFavorites(Video video) {
        List<Video> list = getFavoriteList();
        list.add(video);
        saveFavoriteList(list);
    }

    public static void saveFavoriteList(List<Video> videosList) {
        Hawk.put(FAVORITES, videosList);
    }

    public static List<Video> getFavoriteList() {
        return Hawk.get(FAVORITES, new ArrayList<Video>());
    }

}