package pl.serioteam.hackyeah.goster;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForumActivity extends AppCompatActivity {

  @BindView(R.id.navigationTimeLine)
    ImageView navigateIcon;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;



    VideoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        ButterKnife.bind(this);

        navigateIcon.setImageDrawable(getDrawable(R.drawable.ic_time_line_visible));
    }

    @Override
    protected void onResume() {
        super.onResume();
        initRecycler();
    }

    private void initRecycler() {
        adapter = new VideoAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
         recyclerView.setAdapter(adapter);
        List<Video> list = PreferencesHelper.getVideoList();
        adapter.updateVideo(list);
    }

    @OnClick(R.id.navigationAdd)
    void navigationAdd() {
        navigateToActivity(AddActivity.class);
    }
    @OnClick(R.id.navigationMaps)
    void navigationMaps() {
        navigateToActivity(MapActivity.class);
    }
    @OnClick(R.id.navigationTimeLine)
    void navigationTimeLine() {
        navigateToActivity(ForumActivity.class);
    }
    @OnClick(R.id.navigationFavorite)
    void navigationFavorite() {
        navigateToActivity(FavoriteActivity.class);
    }
    @OnClick(R.id.navigationProfile)
    void navigationProfile() {
        navigateToActivity(ProfileActivity.class);
    }

    private void navigateToActivity(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(intent);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

}
