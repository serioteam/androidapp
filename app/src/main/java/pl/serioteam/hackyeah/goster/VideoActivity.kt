package pl.serioteam.hackyeah.goster

import android.graphics.PixelFormat
import android.graphics.Point
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import pl.serioteam.hackyeah.goster.utils.LocationService
import java.text.DecimalFormat
import java.util.*
import java.util.concurrent.TimeUnit

class VideoActivity : AppCompatActivity() {

    @BindView(R.id.video_view)
    lateinit var videoView: VideoView

    @BindView(R.id.really_speed)
    lateinit var maxSppeed: TextView

    @BindView(R.id.speed_middel)
    lateinit var currentSpeed: TextView

    @BindView(R.id.avg_speed_text)
    lateinit var avgSpeed: TextView

    lateinit var slabo: Disposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)

        ButterKnife.bind(this)
        //        setVideo();

        maxSppeed.text = "0"
        currentSpeed.text = "0"
        avgSpeed.text = "0"

        val rootView = findViewById<View>(R.id.rootLayout) as RelativeLayout
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)

        val rootViewParams = rootView.layoutParams as FrameLayout.LayoutParams
        val videoWidth = 1280
        val videoHeight = 864

        if (videoWidth.toFloat() / videoHeight.toFloat() < size.x.toFloat() / size.y.toFloat()) {
            rootViewParams.width = size.x
            rootViewParams.height = videoHeight * size.x / videoWidth
            rootView.x = 0f
            rootView.y = ((rootViewParams.height - size.y) / 2 * -1).toFloat()
        } else {
            rootViewParams.width = videoWidth * size.y / videoHeight
            rootViewParams.height = size.y
            rootView.x = ((rootViewParams.width - size.x) / 2 * -1).toFloat()
            rootView.y = 0f
        }
        rootView.layoutParams = rootViewParams


        val mVideoView = findViewById<View>(R.id.video_view) as VideoView
        mVideoView.setVideoURI(Uri.parse("android.resource://" + packageName + "/" + R.raw.video02))
        mVideoView.requestFocus()

        val location = LocationService()
        val gps = location.getArrayOfGps()
        val df = DecimalFormat("#.#")
        mVideoView.setOnPreparedListener {
            mVideoView.start()
            var max = Float.MIN_VALUE
            var sum = 0f
            var number = 0
            var current = 0f
            var avg = 0f
            var i = 0

            slabo = Observable.interval(1, TimeUnit.SECONDS, AndroidSchedulers.mainThread()).subscribe({
                current = gps[number]
                max = Math.max(max, current)
                number++
                sum += current
                avg = sum / number
                maxSppeed.text = df.format(max).toString()
                currentSpeed.text = df.format(current).toString()
                avgSpeed.text = df.format(avg).toString()
            },{})
        }

        mVideoView.setOnCompletionListener { slabo.dispose() }

    }

    private fun setVideo() {

        window.setFormat(PixelFormat.TRANSLUCENT)
        val videoHolder = VideoView(this)
        videoHolder.setMediaController(MediaController(this))
        val video = Uri.parse("android.resource://" + packageName + "/"
                + R.raw.video02)
        videoHolder.setVideoURI(video)
        setContentView(videoHolder)
        videoHolder.start()
    }


}
